# State complet pour installer un serveur mysql
# et configurer une base de donnees.
# 
# A configurer dans le pillar /srv/pillar/mysql.sls

include:
  - mysql.python_client
  - mysql.server
  - mysql.setup
