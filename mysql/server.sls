# State pour installer un serveur mysql ou compatible

{% set admin_mail = salt['pillar.get']('mail:admin', 'root') %}

# Installation du paquet et demarrage du service
mysql-server:
  pkg.installed:
    - name :
      - mysql-server
  service.running:
    - name: mysql
    - require:
      - pkg: mysql-server

# Securisation de l'installation

# 1. initialisation d'un compte root@localhost
root@localhost:
  mysql_user.present:
    - name: root
    - host: localhost
    - password: {{ salt['pillar.get']('mysql.pass') }}
    - connection_pass: ""

# 2. suppression de tous les autres root
{% for host in [ salt['grains.get']('host'), '127.0.0.1', '::1'] %}
root@{{ host }}:
  mysql_user.absent:
    - name: root
    - host: {{ host }}
{% endfor %}

