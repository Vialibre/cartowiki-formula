# State pour configurer une base de donnees
#
# A configurer dans /srv/pillar/mysql.sls

{% set admin_mail = salt['pillar.get']('mail:admin', 'root') %}

# Setup database, users, and privileges

# 1. database
{% for db, db_value in salt['pillar.get']('mysql:databases', {}).items() %}
mysql_db_{{db}}:
  mysql_database.present:
    - name: {{db}}
    - host: localhost
    - connection_charset: utf8
{% endfor %}

# 2. users
{% for user, user_value in salt['pillar.get']('mysql:users', {}).items() %}
mysql_user_{{user}}:
  mysql_user.present:
    - name: {{user}}
    - host: {{user_value['host']}}
    - password: {{user_value['password']}}
    - connection_charset: utf8
{% endfor %}

# 3. grants
{% for grant, grant_value in salt['pillar.get']('mysql:grants', {}).items() %}
mysql_grants_{{grant}}:
  mysql_grants.present:
    - grant: {{grant_value['grant']}}
    - database: {{grant_value['database']}}
    - user: {{grant}}
    - host: {{grant_value['host']}}
    - connection_charset: utf8
{% endfor %}
