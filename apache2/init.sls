#
# Apache httpd server
#

{% from "apache2/map.jinja" import apache with context %}

{% set admin_mail = salt['pillar.get']('mail:admin', 'root') %}

apache:
  pkg.installed:
    - name: {{ apache.server }}
  service.running:
    - name: {{ apache.service }}
    - enable: True

apache-modules:
  apache_module.enable:
    - names: {{salt['pillar.get']('http_vhost:modules', {})}}

{% if pillar['http_vhost']['ssl'] %}

apache-module-ssl:
  apache_module.enable:
    - name: ssl

/etc/ssl/certs/{{pillar['http_vhost']['servername']}}:
  file.recurse:
    - source: salt://apache2/certs/{{pillar['http_vhost']['servername']}}
    - user: root
    - group: root
    - dir_mode: 750
    - file_mode: 640
    - require_in:
      - file: /etc/apache2/sites-available/default-ssl

/etc/apache2/sites-available/default-ssl:
  file.managed:
    - source: salt://apache2/files/default-ssl
    - user: root
    - group: root
    - mode: 644
    - template: jinja
    - context:
      servername: {{pillar['http_vhost']['servername']}}
    - watch_in:
      - service: {{ apache.service }}

/etc/apache2/ports.conf:
  file.managed:
    - source: salt://apache2/files/ports.conf
    - template: jinja
    - user: root
    - group: root
    - mode: 644
    - watch_in:
      - service: {{ apache.service }}

/etc/apache2/sites-enabled/000-default:
  file.symlink:
    - target: ../sites-available/default-ssl
    - watch_in:
      - service: {{ apache.service }}

{% endif %}
