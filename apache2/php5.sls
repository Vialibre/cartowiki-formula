{% from "apache2/map.jinja" import apache with context %}

include:
  - apache2

php5:
  pkg.installed:
    - name: php5
    - watch_in:
      - service: {{ apache.service }} 
