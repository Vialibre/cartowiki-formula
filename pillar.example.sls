###
# Pillar to install a full mediawiki stack and all extensions needed by a
# cartowiki instance
###

# Mysql server configuration
############################
mysql:

  # choose a mysql root passwd
  pass: ''

  # the database that will host the mediawiki
  databases:
    cartowiki: [] 

  # the user that will access the cartowiki database
  users :
    cartowiki:
      # no remote access
      host: localhost
      # choose a mysql cartowiki passwd
      password: '' 

  # sets all rights for the new user on its database
  grants :
    cartowiki:
      # no remote access needed
      host: localhost
      grant: all privileges
      database: 'cartowiki.*'

# Apache2 vhost configuration
#############################

http_vhost:
  # listen on hostname by default
  servername: {{ grains['host'] }}
  # ssl disabled for test environment
  ssl: False
  ipv6: False
  ipv4: True
  # rewrite is needed by mediawiki
  modules:
    - rewrite


# Mediawiki configuration
#########################
mediawiki:

  # database credentials
  dbname: cartowiki
  dbuser: cartowiki

  # absolute path to the LocalSettings.php file 
  confpath: /var/www/cartowiki
  # same relative path from apache document root
  scriptpath: /cartowiki

  # choose a passwd (and name) for the admin user
  adminpass: ''
  adminuser: admin

  # the wikiname will be displayed on the top left
  wikiname: Cartowiki

  # mediawiki version to install.
  # if you install from git, it has to be a valid git branch
  # this version will be used by default for all extensions, unless specified
  version: REL1_25

  # download additional extensions
  # the extensions will be installed in the /extensions/ directory.
  extensions:

    # the following will clone branch REL1_25 from the official git repository.
    Arrays: {}
    CategoryTree: {}
    ExternalData: {}
    HeaderTabs: {}
    ImageMap: {}
    Loops: {}
    MassEditRegex: {}
    PageSchemas: {}
    ParserFunctions: {}
    Variables: {}
    DataTransfer: {}

    # you can specify additional packages to install.
    GraphViz: 
      pkgs:
        - graphviz
        - graphviz-dev

    # you can use different repository and branch version
    NativeSvgHandler:
      source: https://github.com/p12tic/NativeSvgHandler.git
      version: master
    RegexParserFunctions:
      source: https://github.com/mediawiki4intranet/RegexParserFunctions.git
      version: REL1_18

    # you can download an additional file
    PlantUML:
      source: https://github.com/pjkersten/PlantUML.git
      version: master
      pkgs:
        - openjdk-7-jre-headless
      files:
        plantuml.jar:
          source: https://downloads.sourceforge.net/project/plantuml/plantuml.jar
          source_hash: sha256=632e57aebd6aa986fb0cf9d80d468abd10d7414f79b4d5637ef2adf0794f6106

    # semantic extensions
    # some need composer to download the correct dependancies
    SemanticMediaWiki:
      type: composer
      name: semantic-media-wiki 
      version: ~2.2
    SemanticResultFormats:
      type: composer
      name: semantic-result-formats
      version: ~2.2
    SemanticForms:
      type: composer
      name: semantic-forms
      version: ~3.3
    SemanticFormsInputs: {}
    SemanticCompoundQueries: {}
    SemanticDrilldown: {}
    SemanticInternalObjects: {}

  # download additional skins
  # the skins will be installed in the /styles/ directory
  skins:
    foreground:
      source: https://github.com/thingles/foreground.git
