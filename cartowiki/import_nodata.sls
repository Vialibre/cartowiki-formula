# State to create a new cartowiki structure with no data 
#
# to create the backup :
# php maintenance/dumpBackup.php  --current --filter=namespace:0,2,4,6,8,10,14,102,106,108 > /tmp/dump-nodata.xml

{% from "mediawiki/map.jinja" import mediawiki with context %}

cartowiki_import_nodata:
  file.recurse:
    - name: /var/lib/cartowiki/dumps
    - source: salt://mediawiki/files/dumps
    - user: root
    - group: root
    - dir_mode: 700
    - file_mode: 600
  cmd.run:
    - name: >
        php maintenance/importDump.php /var/lib/cartowiki/dumps/dump-nodata.xml > /var/lib/cartowiki/importdump.log
        && php maintenance/rebuildrecentchanges.php > /var/lib/cartowiki/rebuildrecentchanges.log
        && touch /var/lib/cartowiki/initialimportdone
    - cwd: {{ mediawiki.confpath }}
    - onlyif: test -f /var/lib/cartowiki/dumps/dump-nodata.xml
    - creates: /var/lib/cartowiki/initialimportdone
