# State to import a full cartowiki backup
#
# to create the backup :
# php maintenance/dumpBackup.php  --current > /tmp/dump-full.xml

{% from "mediawiki/map.jinja" import mediawiki with context %}

cartowiki_import_full:
  file.recurse:
    - name: /var/lib/cartowiki/dumps
    - source: salt://mediawiki/files/dumps
    - user: root
    - group: root
    - dir_mode: 700
    - file_mode: 600
  cmd.run:
    - name: >
        php maintenance/importDump.php /var/lib/cartowiki/dumps/dump-full.xml > /var/lib/cartowiki/importdump.log
        && php maintenance/rebuildrecentchanges.php > /var/lib/cartowiki/rebuildrecentechanges.log
        && touch /var/lib/cartowiki/initialimportdone
    - cwd: {{ mediawiki.confpath }}
    - onlyif: test -f /var/lib/cartowiki/dumps/dump-full.xml
    - creates: /var/lib/cartowiki/initialimportdone
