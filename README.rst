===========================
Salt Formulas for CartoWiki
===========================

These formulas can be used to install a CartoWiki extension on top of a
complete mediawiki stack composed of a mysql database, an apache server,
and a mediawiki installation.

.. contents::
    :local:

Mysql states
============

``mysql``
---------

Runs the states to install a fully operational mysql server, create a
new database, the users and all necessary access rights.

``mysql.python_client``
-----------------------

Installs the python bindings needed by the other salt states.

``mysql.server``
----------------

Installs an empty mysql server and cleans up all unnecessary access
rights.

``mysql.setup``

Sets up a new database, user and privileges, as defined in a pillar
file.

Apache 2 states
===============

``apache2``
-----------

Installs an apache2 server with vhosts, ssl configuration and
certificates, as specified in the pillar file.

``apache2.php5``
----------------

Adds php5 extension to the apache2 server.

Mediawiki states
================

``mediawiki``
-------------

Runs the states to install the packages required, install mediawiki,
its extensions, configure the extensions, and run maintenance.

``mediawiki.cartowiki_import_full``
-----------------------------------

Import a full cartowiki to your mediawiki.

``mediawiki.cartowiki_import_nodata``
-------------------------------------

Imports an empty structure for a new cartowiki setup.

``mediawiki.ext_cartowiki``
---------------------------

Install cartowiki specific extensions, using git, composer or files.

``mediawiki.ext_generic``
-------------------------

Installs the extensions defined in the pillar.

``mediawiki.ext_skins``
-----------------------

Installs additional skins.

``mediawiki.install_core``
--------------------------

Installs a minimal mediawiki from git (core + vendor + Vector skin).

``mediawiki.install_extensions``
--------------------------------

Installs and enables all extensions available (generic + skins + cartowiki)

``mediawiki.install_requirements``
----------------------------------

Installs the external packages required by mediawiki.

``mediawiki.run_firstinstall``
------------------------------

Runs first-time installer to create the database, and initial configuration.
LocalSettings.php file is created at this step.

``mediawiki.run_maintenance``
-----------------------------

Runs the maintenance update script needed after changes.

CartoWiki states
================

``cartowiki.import_full.sls``
-----------------------------

Imports a full CartoWiki into the mediawiki.
You will need to have created an export from another instance as
specified in ``dumps/README.md``.

``cartowiki.import_nodata.sls``
-------------------------------

This import only creates the namespaces and data structure. Use it if
you want to start a new CartoWiki from an empty template. 
