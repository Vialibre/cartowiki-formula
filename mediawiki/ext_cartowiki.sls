# State to install semantic extensions to mediawiki (including smw and the following list) :

{% from "mediawiki/map.jinja" import mediawiki with context %}

#
# Cartowiki specific extensions
#

carto-formats:
  archive.extracted:
    - name: {{ mediawiki.confpath }}/extensions/
    - source: salt://mediawiki/files/extensions/CartoFormats.tar.gz
    - archive_format: tar
    - archive_user: www-data
    - if_missing: {{ mediawiki.confpath }}/extensions/CartoFormats
