# State to run update and maintenance
# It has be run post-install and after each change
#

{% from "mediawiki/map.jinja" import mediawiki with context %}

{% set admin_mail = salt['pillar.get']('mail:admin', 'root') %}

mediawiki-maintenance:
  cmd.run:
    - name: php maintenance/update.php --quick
    - cwd: {{ mediawiki.confpath }}
