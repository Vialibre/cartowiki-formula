# State to install semantic extensions to mediawiki (including smw and the following list) :

{% from "mediawiki/map.jinja" import mediawiki with context %}

#
# Skin Foreground
#

install-skin-foreground:
  git.latest:
    - name: https://github.com/thingles/foreground.git
    - target: {{ mediawiki.confpath }}/skins/foreground
