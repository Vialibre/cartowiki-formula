# State to initialize a fresh mediawiki installation
#

{% from "mediawiki/map.jinja" import mediawiki with context %}

{% set admin_mail = salt['pillar.get']('mail:admin', 'root') %}

mediawiki-first_install:
  cmd.run:
    - name: >
        php maintenance/install.php
        --server "http://{{grains['host']}}"
        --dbserver {{ mediawiki.dbserver }}
        --dbtype {{ mediawiki.dbtype }}
        --lang {{ mediawiki.lang }}
        --confpath {{ mediawiki.confpath }}
        --dbname {{ mediawiki.dbname }}
        --dbpass "{{ salt.pillar.get('mysql:users:cartowiki:password', '') }}"
        --dbuser {{ mediawiki.dbuser }}
        --scriptpath {{ mediawiki.scriptpath }}
        --pass {{ mediawiki.adminpass }}
        "{{ mediawiki.wikiname }}" {{ mediawiki.adminuser }}
    - cwd: {{ mediawiki.confpath }}
    - creates: {{ mediawiki.confpath }}/LocalSettings.php

LocalSettings.d/:
  file.directory:
    - name: {{ mediawiki.confpath }}/LocalSettings.d/

LocalSettings.php:
  file.append:
    - name: {{ mediawiki.confpath }}/LocalSettings.php
    - text: |
        foreach (glob("LocalSettings.d/*.php") as $filename)
        {
            include $filename;
        }    
    - onlyif: test -f {{ mediawiki.confpath }}/LocalSettings.php
