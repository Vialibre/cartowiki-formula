# State to install semantic extensions to mediawiki (including smw and the following list) :
#   - semantic-media-wiki
#   - semantic-result-formats
#   - semantic-forms
#   - semantic-compound-queries
#   - semantic-drilldown
#   - semantic-forms-inputs
#   - semantic-internal-objects

{% from "mediawiki/map.jinja" import mediawiki with context %}

semantic-media-wiki:
  cmd.run:
    - name: COMPOSER_HOME={{ mediawiki.confpath }} composer require mediawiki/semantic-media-wiki "~2.1"
    - cwd: {{ mediawiki.confpath }} 

semantic-result-formats:
  cmd.run:
    - name: COMPOSER_HOME={{ mediawiki.confpath }} composer require mediawiki/semantic-result-formats "~2.1"
    - cwd: {{ mediawiki.confpath }}

semantic-forms:
  cmd.run:
    - name: COMPOSER_HOME={{ mediawiki.confpath }} composer require mediawiki/semantic-forms "~3.2"
    - cwd: {{ mediawiki.confpath }}

semantic-compound-queries:
  git.latest:
    - name: https://gerrit.wikimedia.org/r/p/mediawiki/extensions/SemanticCompoundQueries.git
    - target: {{ mediawiki.confpath }}/extensions/SemanticCompoundQueries

semantic-drilldown:
  git.latest:
    - name: https://gerrit.wikimedia.org/r/p/mediawiki/extensions/SemanticDrilldown.git
    - target: {{ mediawiki.confpath }}/extensions/SemanticDrilldown

semantic-forms-inputs:
  git.latest:
    - name: https://gerrit.wikimedia.org/r/p/mediawiki/extensions/SemanticFormsInputs.git
    - target: {{ mediawiki.confpath }}/extensions/SemanticFormsInputs

semantic-internal-objects:
  git.latest:
    - name: https://gerrit.wikimedia.org/r/p/mediawiki/extensions/SemanticInternalObjects.git
    - target: {{ mediawiki.confpath }}/extensions/SemanticInternalObjects
