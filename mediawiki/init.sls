# State to install a complete mediawiki and extensions
#
# See pillar.example for a configuration template.

{% from "mediawiki/map.jinja" import mediawiki with context %}

{% set admin_mail = salt['pillar.get']('mail:admin', 'root') %}

include:
  - mediawiki.install_requirements
  - mediawiki.install_core
  - mediawiki.run_firstinstall
  - mediawiki.install_extensions
  - mediawiki.run_maintenance
