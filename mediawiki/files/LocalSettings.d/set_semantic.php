<?php
//
// Settings for semantic extensions
//

$wgLanguageCode = "fr";

// enable
enableSemantics( parse_url( $wgServer, PHP_URL_HOST ) );
require_once("$IP/extensions/SemanticCompoundQueries/SemanticCompoundQueries.php");
require_once("$IP/extensions/SemanticDrilldown/SemanticDrilldown.php");
require_once("$IP/extensions/SemanticFormsInputs/SemanticFormsInputs.php");
require_once("$IP/extensions/SemanticInternalObjects/SemanticInternalObjects.php");

// drilldown settings
$sdgNumResultsPerPage=500;
$sdgHideCategoriesByDefault = true;
$sdgMinValuesForComboBox=0;
