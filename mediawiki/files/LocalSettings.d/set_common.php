<?php
//
// Common settings for mediawiki
//

$wgLanguageCode = "fr";
$wgUseAjax = true;
$wgEnableUploads = true; # Enable uploads
$wgPhpCli = false;

// enable massedit
require_once ( "$IP/extensions/MassEditRegex/MassEditRegex.php" );
$wgGroupPermissions['sysop']['masseditregex'] = true; // Allow administrators to use Special:MassEditRegex

// set debug mode
// uncomment the following lines to enable
$wgDebugLogFile = "/var/log/mediawiki/debug-{$wgDBname}.log";
$wgDebugToolbar = true;
$wgShowExceptionDetails = true;
$wgDevelopmentWarnings = true;
