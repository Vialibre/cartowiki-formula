<?php
//
// Settings for syntax extensions
//

// enable
require_once("$IP/extensions/Arrays/Arrays.php");
require_once("$IP/extensions/CategoryTree/CategoryTree.php");
require_once("$IP/extensions/ExternalData/ExternalData.php");
require_once "$IP/extensions/DataTransfer/DataTransfer.php";
require_once("$IP/extensions/Loops/Loops.php");
require_once("$IP/extensions/PageSchemas/PageSchemas.php");
require_once("$IP/extensions/ParserFunctions/ParserFunctions.php");
require_once("$IP/extensions/RegexParserFunctions/RegexParserFunctions.php");
require_once("$IP/extensions/Variables/Variables.php");
$wgPFEnableStringFunctions = true;
