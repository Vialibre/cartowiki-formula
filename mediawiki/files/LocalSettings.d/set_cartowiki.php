<?php
//
// Settings for cartowiki
//

// enable
require_once("$IP/extensions/CartoWiki/CartoWiki.php");

// namespaces
$wgExtraNamespaces[3000] = "Application";
$wgExtraNamespaces[3001] = "Application_Discussion";   // underscore required
$wgContentNamespaces[] = 3000;
$smwgNamespacesWithSemanticLinks[3000] = true;
$wgExtraNamespaces[3002] = "Composant_logiciel";
$wgExtraNamespaces[3003] = "Composant_logiciel_Discussion";   // underscore required
$wgContentNamespaces[] = 3002;
$smwgNamespacesWithSemanticLinks[3002] = true;
$wgExtraNamespaces[3004] = "Matériel";
$wgExtraNamespaces[3005] = "Matériel_Discussion";   // underscore required
$wgContentNamespaces[] = 3004;
$smwgNamespacesWithSemanticLinks[3004] = true;
$wgExtraNamespaces[3006] = "Personne";
$wgExtraNamespaces[3007] = "Personne_Discussion";   // underscore required
$wgContentNamespaces[] = 3006;
$smwgNamespacesWithSemanticLinks[3006] = true;
$wgExtraNamespaces[3008] = "Processus";
$wgExtraNamespaces[3009] = "Processus_Discussion";   // underscore required
$wgContentNamespaces[] = 3008;
$smwgNamespacesWithSemanticLinks[3008] = true;
$wgExtraNamespaces[3010] = "Flux";
$wgExtraNamespaces[3011] = "Flux_Discussion";   // underscore required
$wgContentNamespaces[] = 3010;
$smwgNamespacesWithSemanticLinks[3010] = true;
$wgExtraNamespaces[3012] = "Salle_informatique";
$wgExtraNamespaces[3013] = "Salle_informatique_Discussion";   // underscore required
$wgContentNamespaces[] = 3012;
$smwgNamespacesWithSemanticLinks[3012] = true;
$wgExtraNamespaces[3014] = "Baie";
$wgExtraNamespaces[3015] = "Baie_Discussion";   // underscore required
$wgContentNamespaces[] = 3014;
$smwgNamespacesWithSemanticLinks[3014] = true;
$wgExtraNamespaces[3016] = "Structure";
$wgExtraNamespaces[3017] = "Structure_Discussion";   // underscore required
$wgContentNamespaces[] = 3016;
$smwgNamespacesWithSemanticLinks[3016] = true;
$wgExtraNamespaces[3018] = "Métier";
$wgExtraNamespaces[3019] = "Métier_Discussion";   // underscore required
$wgContentNamespaces[] = 3018;
$smwgNamespacesWithSemanticLinks[3018] = true;
$wgExtraNamespaces[3020] = "Objectif";
$wgExtraNamespaces[3021] = "Objectif_Discussion";   // underscore required
$wgContentNamespaces[] = 3020;
$smwgNamespacesWithSemanticLinks[3020] = true;
$wgExtraNamespaces[3022] = "Donnée";
$wgExtraNamespaces[3023] = "Donnée_Discussion";   // underscore required
$wgContentNamespaces[] = 3022;
$smwgNamespacesWithSemanticLinks[3022] = true;
$wgExtraNamespaces[3024] = "Lien_réseau";
$wgExtraNamespaces[3025] = "Lien_réseau_discussion";   // underscore required
$wgContentNamespaces[] = 3024;
$smwgNamespacesWithSemanticLinks[3024] = true;
