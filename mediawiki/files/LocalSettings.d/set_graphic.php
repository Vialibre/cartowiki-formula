<?php
//
// Settings for graphic extensions
//

// image map
require_once("$IP/extensions/ImageMap/ImageMap.php");

// plantuml
require_once("$IP/extensions/PlantUML/PlantUML.php");
$plantumlImagetype = 'svg';

// native SVG support
require_once "$IP/extensions/NativeSvgHandler/NativeSvgHandler.php";
$wgNativeSvgHandlerEnableLinks = true; //Set to false to disable links over SVG images
$wgFileExtensions[] = 'svg';
$wgSVGConverter = 'ImageMagick';
