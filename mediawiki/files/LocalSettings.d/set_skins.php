<?php
//
// Skin settings
//

// choose foreground skin 
require_once( "$IP/skins/foreground/foreground.php" );
$wgDefaultSkin = "foreground";
$wgForegroundFeatures = array(
  'showActionsForAnon' => true,
  'NavWrapperType' => 'divonly',
  'showHelpUnderTools' => true,
  'showRecentChangesUnderTools' => true,
  'wikiName' => &$GLOBALS['wgSitename'],
  'navbarIcon' => true,
  'IeEdgeCode' => 1,
  'showFooterIcons' => 0,
  'addThisFollowPUBID' => ''
  );

// edit tabs extension
require_once( "$IP/extensions/HeaderTabs/HeaderTabs.php" );
$sfgRenameEditTabs = true;
