# State to install mediawiki raw from source
#

{% from "mediawiki/map.jinja" import mediawiki with context %}

{% set admin_mail = salt['pillar.get']('mail:admin', 'root') %}

include:
  - mediawiki.install_requirements

# Install from git
# Reference: https://www.mediawiki.org/wiki/Download_from_Git
mediawiki-core:
  git.latest:
    - name: https://gerrit.wikimedia.org/r/p/mediawiki/core.git
    - target: {{ mediawiki.confpath }}
    - rev: {{ mediawiki.version }} 

# Starting with MediaWiki 1.25, some required external libraries moved from "core" to "vendor".
mediawiki-vendor:
  git.latest:
    - name: https://gerrit.wikimedia.org/r/p/mediawiki/vendor.git
    - target: {{ mediawiki.confpath }}/vendor
    - rev: {{ mediawiki.version }} 

# The default skin is called Vector. We need it as well.
mediawiki-skins:
  git.latest:
    - name: https://gerrit.wikimedia.org/r/mediawiki/skins/Vector
    - target: {{ mediawiki.confpath }}/skins/Vector
    - rev: {{ mediawiki.version }} 

# All files can be read-only except the upload directory called "images".
mediawiki-upload-permissions:
  file.directory:
    - name: {{ mediawiki.confpath }}/images/
    - user: www-data
    - group: www-data
    - recurse:
      - user
      - group
