# State to install extensions to mediawiki
# 
# Use pillars to define the extensions list

{% from "mediawiki/map.jinja" import mediawiki with context %}

{% for id in mediawiki.extensions %}

# Get the type of install (default = git)
{% set type = mediawiki.extensions[id].get('type', 'git') %}
# Get the name of package (default = same as id)
{% set name = mediawiki.extensions[id].get('name', id) %}
# Get the extension version (default is inherited from mediawiki)
{% set version = mediawiki.extensions[id].get('version', mediawiki.version ) %}
# Get git source (default source is constructed as a standard gerrit.wikimedia.org url)
{% set source = mediawiki.extensions[id].get('source', "https://gerrit.wikimedia.org/r/p/mediawiki/extensions/{}.git".format(name)) %}
# Get additional packages to install
{% set pkgs = mediawiki.extensions[id].get('pkgs', {}) %}
# Get additional files to download
{% set files = mediawiki.extensions[id].get('files', {}) %}

# If type is git, clone from source
{% if type == 'git' %}
mediawiki_extension_{{ id }}:
  git.latest:
    - name: {{ source }}
    - target: {{ mediawiki.confpath }}/extensions/{{ name }}
    - rev: {{ version}}
{% endif %}

# If type is composer, run the command
{% if type == 'composer' %}
mediawiki_extension_{{ id }}:
  cmd.run:
    - name: COMPOSER_HOME={{ mediawiki.confpath }} composer require mediawiki/{{ name }} "{{ version }}"
    - cwd: {{ mediawiki.confpath }} 
{% endif %}

# Install additional packages
{% for pkg in pkgs %}
mediawiki_extension_{{ id }}_{{ pkg }}:
  pkg.installed:
    - name: {{ pkg }}
{% endfor %}

# Download additional files
{% for file_id in files %}
mediawiki_extension_{{ id }}_{{ file_id }}:
  file.managed:
    - name: {{ mediawiki.confpath }}/extensions/{{ id }}/{{ file_id }}
    - source: {{ files[file_id].get('source', '') }}
    - source_hash: {{ files[file_id].get('source_hash', '') }}
{% endfor %}

{% endfor %}
