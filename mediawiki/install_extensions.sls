# State to install semantic extensions to mediawiki (including smw and the following list) :

{% from "mediawiki/map.jinja" import mediawiki with context %}

# install all extensions available
include:
#  - .ext_skins
#  - .ext_syntax
#  - .ext_semantic
  - mediawiki.ext_generic
  - mediawiki.ext_cartowiki
  - mediawiki.ext_skins

# enable them
mediawiki-enable_extensions:
  file.recurse:
    - name: {{ mediawiki.confpath }}/LocalSettings.d/
    - source: salt://mediawiki/files/LocalSettings.d/
    - template: jinja
    - clean: True
