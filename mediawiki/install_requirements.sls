# State to install all mediawiki required packets
#

{% set admin_mail = salt['pillar.get']('mail:admin', 'root') %}

mediawiki_requirements:
  pkg.installed:
    - pkgs:
      - git
      - curl
      - php5
      - php5-mysql
      - php5-intl
      - graphviz
      - imagemagick

mediawiki-composer:
  cmd.run:
    - name: 'curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/bin --filename=composer'
    - creates: /usr/bin/composer
    - cwd: /root/
